package erasmus;

public class App {

	public static void main(String[] args) throws Exception {
		doDatabaseStuff();
	}
	
	private static void doDatabaseStuff() throws Exception
	{
		DataSource dataSource = new DataSource();

		try {
			dataSource.openConnection();
			System.out.println("Connection opened!");
			dataSource.setDatabaseCredentials("./src/erasmus/database.properties");
			System.out.println("Database credentials set!");
		} finally {
			dataSource.closeConnection();
			System.out.println("Connection closed!");
		}
	}
}
