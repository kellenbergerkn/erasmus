package erasmus;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataSource {

	private Connection connection;

	private String host = "192.168.1.22";
	private String port = "3306";
	private String databaseName = "e3fieu";

	private String user = "e3fieu";
	private String password = "eu27112018";

	public void setDatabaseCredentials(String filePath) throws FileNotFoundException, IOException {
		PropertiesProvider propertiesProvider = new PropertiesProvider();
		Properties properties = propertiesProvider.getProperties(filePath);

		host = properties.getProperty("database.host");
		port = properties.getProperty("database.port");
		databaseName = properties.getProperty("database.dbName");
		user = properties.getProperty("database.user");
		password = properties.getProperty("database.password");
	}

	private String getConnectionString() {
		return String.format("jdbc:mysql://%s:%s/%s", host, port, databaseName);
	}

	public void openConnection() throws SQLException, ReflectiveOperationException {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

		String connectionString = getConnectionString();
		System.out.println(connectionString);
		connection = DriverManager.getConnection(getConnectionString(), user, password);
	}

	public void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
		}
	}
}