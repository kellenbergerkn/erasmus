package erasmus;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesProvider 
{
	public Properties getProperties(String filePath) throws FileNotFoundException, IOException
	{
		Properties properties = new Properties();
		 
		try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(filePath)))) 
		{
		  properties.load(bis);
		  return properties;
		}
	}
}
